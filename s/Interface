; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        SUBT    NFS<->FileSwitch interface code

        GET   Hdr:ListOpts
        GET   Hdr:Macros
        GET   Hdr:System
        GET   Hdr:Machine.<Machine>
        GET   Hdr:APCS.<APCS>

        GBLL    ProfileIt
ProfileIt       SETL    {FALSE}

        IMPORT  |Image$$RO$$Base|
        IMPORT  |_Lib$Reloc$Off$DP|
        IMPORT  fsentry_open
        IMPORT  fsentry_getbytes
        IMPORT  fsentry_putbytes
        IMPORT  fsentry_args
        IMPORT  fsentry_close
        IMPORT  fsentry_file
        IMPORT  fsentry_func
        IMPORT  fsentry_gbpb

        EXPORT  veneer_fsentry_open
        EXPORT  veneer_fsentry_getbytes
        EXPORT  veneer_fsentry_putbytes
        EXPORT  veneer_fsentry_args
        EXPORT  veneer_fsentry_close
        EXPORT  veneer_fsentry_file
        EXPORT  veneer_fsentry_func
        EXPORT  veneer_fsentry_gbpb

        AREA    FSEntry_Interfaces,REL,CODE,READONLY

        LTORG

veneer_fsentry_open
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*0
        B       fsentry_common
veneer_fsentry_getbytes
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*1
        B       fsentry_common
veneer_fsentry_putbytes
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*2
        B       fsentry_common
veneer_fsentry_args
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*3
        B       fsentry_common
veneer_fsentry_close
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*4
        B       fsentry_common
veneer_fsentry_file
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*5
        B       fsentry_common
veneer_fsentry_func
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*6
        B       fsentry_common
veneer_fsentry_gbpb
        Push    "r8"
        MOV     r8, #fsentry_branchtable - %F10 + 4*7
        B       fsentry_common

fsentry_common  ; os_error *fsentry_common( Parameter_Block * )

        ; Store the input registers onto the stack
        Push    "r0-r7, sl, fp, ip, lr"

        MOV     sl, sp, LSR #20
        MOV     sl, sl, LSL #20         ; SP_LWM
        LDMIA   sl, {v1, v2}            ; save old reloc modifiers over fn call
        LDR     r12, [r12]              ; private word pointer
        LDMIB   r12, {fp, r12}          ; new relocation modifiers
        STMIA   sl,  {fp, r12}          ; set by module init
        MOV     fp, #0                  ; halt C backtrace here!

        ; This is equivalent of 'ADD r10, r10, #0' + |_Lib$Reloc$Off$DP|
        DCD     |_Lib$Reloc$Off$DP| + &E28AA000

        ; Pass a pointer to the structure on the stack
        MOV     a1, sp

 [ ProfileIt
        Push    "r0,r1"
        MOV     r1, #1          ; In
        SWI     &c0103          ; Profiler_SetIndex
        Pull    "r0,r1"
 ]

        ; BL    fsentry_branchtable[r8]
        MOV     lr, pc
        ADD     pc, pc, r8

        ; This is equivalent of 'SUB r10, r10, #0' + |_Lib$Reloc$Off$DP|
        DCD     |_Lib$Reloc$Off$DP| + &E24AA000

10      ; This label must be the 2nd instructions past the above ADD pc, pc, r8

 [ ProfileIt
        Push    "r0,r1"
        MOV     r1, #0          ; Out
        SWI     &c0103          ; Profiler_SetIndex
        Pull    "r0,r1"
 ]

        STMIA   sl, {v1, v2}            ; restore old reloc modifiers

        ; Save the returned value in r8
        MOVS    r8, r0
        ; Get the stuff off the stack
        Pull    "r0-r7, sl, fp, ip, lr"
        ; If returned value indicates an error, then set the overflow and put it back in r0
        MOVNE   r0, r8

        ; Mess about with the flag bits in R8
        [ :LNOT: No32bitCode
        ; If we're allowed to use 32-bit code, it all falls out really nicely
        MOVNE   R8, #V_bit              ; R8 == 0 if Z, V set if !Z
        |
        MOV     R8, pc
        BIC     R8, R8, #C_bit + V_bit
        ORRNE   R8, R8, #V_bit          ; V = err != 0
        ]
        TST     r1, r1                  ; C = r1 == 0
        ORREQ   R8, R8, #C_bit

        ; Move the flag bits into psr
        [ :LNOT: No32bitCode
        MSR     CPSR_f, r8
        |
        TEQP    R8, #0
        NOP
        ]

        Pull    "r8"

        MOV     pc, lr

fsentry_branchtable
        B       fsentry_open
        B       fsentry_getbytes
        B       fsentry_putbytes
        B       fsentry_args
        B       fsentry_close
        B       fsentry_file
        B       fsentry_func
        B       fsentry_gbpb

        END
